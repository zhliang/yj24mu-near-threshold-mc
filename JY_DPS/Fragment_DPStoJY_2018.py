import FWCore.ParameterSet.Config as cms
from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *

FourMuonFilter = cms.EDFilter("FourLepFilter", # require 4-mu in the final state
        MinPt = cms.untracked.double(1.8),
        MaxPt = cms.untracked.double(4000.0),
        MaxEta = cms.untracked.double(2.5),
        MinEta = cms.untracked.double(0.),
        ParticleID = cms.untracked.int32(13)
)

JpsiUpsilonFilter = cms.EDFilter("MCParticlePairFilter",  # require 2-mu mass to be 8.5 - 11.5 GeV
        MinPt = cms.untracked.vdouble(1.0,1.0),
        MaxPt = cms.untracked.vdouble(4000.0,4000.0),
        MaxEta = cms.untracked.vdouble( 2.5,2.5),
        MinEta = cms.untracked.vdouble(-2.5,-2.5),
        ParticleID1 = cms.untracked.vint32(553, -553),
        ParticleID2 = cms.untracked.vint32(443, -443),
        MinInvMass = cms.untracked.double(1.0),
        MaxInvMass = cms.untracked.double(16.0),
)

generator = cms.EDFilter("Pythia8GeneratorFilter",
    crossSection = cms.untracked.double(1.0),
    PythiaParameters = cms.PSet(
        pythia8CommonSettingsBlock,
        pythia8CP5SettingsBlock,
        processParameters = cms.vstring(
            'Charmonium:gg2ccbar(3S1)[3S1(1)]g = on,off',
            'Charmonium:gg2ccbar(3S1)[3S1(1)]gm = on, off',
            'Charmonium:gg2ccbar(3S1)[3S1(8)]g = on, off',
            'Charmonium:qqbar2ccbar(3S1)[3S1(8)]g = on,off',
            'Charmonium:qg2ccbar(3S1)[3S1(8)]q = on,off',
            'Charmonium:qqbar2ccbar(3S1)[3S1(8)]g = on,off',
            'Charmonium:gg2ccbar(3S1)[1S0(8)]g = on,off',
            'Charmonium:qg2ccbar(3S1)[1S0(8)]q = on,off',
            'Charmonium:qqbar2ccbar(3S1)[1S0(8)]g = on,off',
            'Charmonium:gg2ccbar(3S1)[3PJ(8)]g = on,off',
            'Charmonium:qg2ccbar(3S1)[3PJ(8)]q = on,off',
            'Charmonium:qqbar2ccbar(3S1)[3PJ(8)]g = on,off',
#           'Charmonium:all = on',
            'SecondHard:generate = on',
            'SecondHard:bottomonium = on',
#           'PhaseSpace:mHatMin = 10.',
            #'PhaseSpace:pTHatMin = 1.',
#           'PhaseSpace:pTHatMinSecond = 1.',
#           'PhaseSpace:pTHatMinDiverge = 0.1',
            '100443:onMode = off', # Others
            '10441:onMode = off',
            '20443:onMode = off',
            '443:onMode = off',
            '443:onIfMatch = -13 13',
            '100553:onMode = off',
            '200553:onMode = off',
            '10551:onMode = off',
            '20553:onMode = off',
            '555:onMode = off',
            '553:onMode = off',
            '553:onIfMatch = -13 13',
            'PartonLevel:MPI = on',              #! no multiparton interactions
            'PartonLevel:ISR = on',              #! no initial-state radiation
            'PartonLevel:FSR = on',              #! no final-state radiation
            'HadronLevel:all = on',              #! continue after parton level
            'HadronLevel:Hadronize = on',        #! no hadronization
            'HadronLevel:Decay = on',            #! no decays
        ),
        parameterSets = cms.vstring(
            'pythia8CommonSettings', 
            'pythia8CP5Settings', 
            'processParameters'
        ),  
    ),
    comEnergy = cms.double(13000.0),
    maxEventsToPrint = cms.untracked.int32(0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    pythiaPylistVerbosity = cms.untracked.int32(1)
)

ProductionFilterSequence = cms.Sequence(generator*FourMuonFilter)